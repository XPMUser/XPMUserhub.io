# XPMUser.github.io

Warning: Please don't go on sites that were made from Byethost on any school or work devices that are owned by the school or work companies! Please be careful when visiting/using the sites from Byethost.

Click on the left arrow to go back.
Credits: Sitemakers: Daboss7173 
Prodigy Modifiers: Slappygig, XPMUser/Ao28th28, Cookie, Daboss7173, and more
Reborn maker: Craftersshaft
Prodigy Maker: Prodigy Education

Song Makers: Jamie Christopherson and Matthew Pablo
Song Editors: Prodigy Education a.k.a Prodigy Game
Font Makers: Prodigy Education and more
Playtesters: Daboss7173, XPMUser/Ao28th28, Mr. Inquiry and more
Pictures: Prodigy Education and the mustache creator.

Introducers: Ao28th28 a.k.a XPMUser, Mr. Inquiry, and more

Original Prodigy Makers: Rohan Mahimker/Alexander Peters

I gave credits above.

XPMUser is the default username of an account to use in Windows XP Mode in Windows 7.
I'm Ao28th28 (The PVZ Unknown Edition Maker).

Old Prodigy English is coming soon!

# Table of Contents 
1. XPMUser.github.io https://github.com/XPMUser/XPMUser.github.io/edit/main/README.md#xpmusergithubio
2. Table of Contents https://github.com/XPMUser/XPMUser.github.io/edit/main/README.md#table-of-contents
3. Old Prodigy https://github.com/XPMUser/XPMUser.github.io/edit/main/README.md#old-prodigy
4. How to play https://github.com/XPMUser/XPMUser.github.io/edit/main/README.md#how-to-play
5. Modpacks https://github.com/XPMUser/XPMUser.github.io?tab=readme-ov-file#modpacks
6. List of Versions https://github.com/XPMUser/XPMUser.github.io/edit/main/README.md#list-of-versions
   
# Old Prodigy

This is some pretty simple modding of older versions of Prodigy, to get them to work right now, in the present day!
If you want a little dose of nostalgia from older Prodigy versions, no matter what the purpose - 
seeing the older battle systems, the older quests, or anything of the sort - 
then you know where you are, and you know what to do!

There are a lot of versions to choose from based on your preferences, covering YEARS worth of updates from Prodigy's team!

I am still actively working on bettering it, so if you have any problems with bugs, glitches, etc. Just make an issue
and I'll try to address it as soon as I can!

There are also forks of my mods. Check them out whenever you get the chance! They're awesome!

## How to play

Playing this couldn't be more simple. First thing you have to do is open the site, which is automatically updated from this
repository!

To do so, you just click this link: https://xpmuser.github.io/oldprodigy/select

Once you're there, you can select a version to play. There are lots of versions to choose from, so try not to be overwhelmed
by the number of choices, and there are still more versions and updates to come!

Once you've selected a version, it'll take you to the login screen where you have 3 options. To create a new wizard, to start in offline mode,
or to load a previous save.

If you want to start a quick game and not configure how your character appears, you should choose offline mode, but in Spring 2015 versions, you have to click on the new wizard button and then the generate random button.

If you want to start a new game and configure your character however you want, you should choose to create a new character.

If you want to load a character that you've already saved, you click load character and select your save file.

To save your character, you can just go into the settings menu, go under 'network' and click the button called "save character". It'll automatically download
to your computer, For this I recommend manually choosing a folder to save it in so you can organize it as you need. If there is no network tab in the version
you're playing in, click the 'other' tab instead.

## Modpacks

This repository features dynamic modpacks, you will be able to toggle mods as you see fit, and be able to combine multiple mods together for dynamic modpacks.

Currently, these are the versions that support modpacks:
 - pde2016 (Prodigy Definitive Edition/Modified 1-30-0)
 - pde2019 (Prodigy Definitive Edition/Modified 2-45-0)
 - 1-28-1 Beta (Not marked as beta in the version selection screen!)
 - 1-29-0
 - 1-30-0
 - My Ultimate Edition: http://opxpmuser.byethost7.com/, http://opxpmuser.byethost7.com/?mods=WalkSpeed/, http://opxpmuser.byethost7.com/?mods=FastGameSpeed/, http://opxpmuser.byethost7.com/?mods=WalkSpeed,FastGameSpeed/
 - My Ultimate Edition (I'm at the Robolymics doing 2014-2015's quests!): http://op2017.byethost32.com/, http://op2017.byethost32.com/?mods=WalkSpeed/, http://op2017.byethost32.com/?mods=FastGameSpeed/, http://op2017.byethost32.com/?mods=WalkSpeed,FastGameSpeed/ 
 - pdenot1.50.0 (Prodigy Definitive Edition/Modified 1-46-2)
 - 1-50-0
 - 2-45-0
 - 2-50-0
 - 1-30-R (Prodigy Reborn 1.0)

## List of Versions

List of versions:

- 0.0.0 | 2014!

  I forgot to make the fonts the same as the one from mid-2014.

- 1.0.0 | TBA!

  No house, skywatch or the dark tower found in this version!

- 1.2.0 | TBA!

  This doesn't even have the Dark Tower.

- 1.3.1 | TBA!

  This might be the last version without the house.

- 1.5.0 | TBA!

  This was the last version to have the Leaderboards available and to name the Barnacle Cove, 'Pirate Isle'.

- 1.6.0 | TBA!

  This has the 1-11-0's quests thing. The leaderboards can't be viewed yet. Pirate Isle's name is Barnacle Cove. The actual one doesn't have the transformotron.

- 1.7.0 | TBA!

  (Need to add)

- 1.8.0 | TBA!

  (Need to add)

- 1.8.9 | Early 2015 Beta Testing!

  This was supposed to be in early 2015.

- 1.9.0 | March 2015

  This version is really special, because it's the oldest one on this site! This version features a tutorial that is different from all of these versions! You start off in the Academy, but then it falls under attack! You help defend the academy by battling off some of the monsters that are attacking the Academy! The wizard maker is also different here! You also setup which pet you start with in battle just before the battle starts, rather than in the pet menu.

- 1.10.0 | March 2015

  This was exactly the same as 1.9.0. This is also the oldest version that has been preserved via the Internet Archive.

- 1.11.0 | April 2015

  This is the first version with Clockwork Town.

- 1.12.2 | The older version that can run essentially flawlessly.

  Now don't get me wrong, this isn't the oldest version of Prodigy ever, but it's pretty old! Lots of old elements around that aren't seen in newer versions.
  
- 1.13.0 | The older version with all of the legacy quests, and the element selection.

  This is a version that I recommend in particular, it's kind of like 1.12.2 - but it's generally a lot more stable. In fact this version
  is one of the most stable Prodigy versions on this site!

- 1.16.2 | The same as 1.16.5 1!

  TBA
  
- 1.16.5 | The same as 1.16.5 2!

  TBA

- 1.16.6 | The same as 1.16.5 3!

  TBA

- 1.17.0 | The older version with tileset-style maps!

  This version was the first to introduce an old tutorial from 9/8/2015, more in-depth than 1.16.5 and before, which would end up being used all the way up to 1.30.0!
  Not only did this version have that, but it also introduced the precursor to the modern firefly forest (before they updated the tileset and updated Flora's sprite).

- 1.17.5 | 1-17-0!

  This is the same as in 1-17-0!
  
- 1.18.0 | All Hallow's Eve!

  This version is really neat because it has the first Pumpkinfest! This means Halloween decorations are all around the map, and there's a Pumpkinfest shop, with all-kinds of exclusive, creepy items and gear you can buy with your gold! Other than that it's pretty much just like 1.17.0.

- 1.18.2 | 1.18.0!

  This is the same as 1.18.0.

- 1.18.3 | TBA!

  Pumpkinfest is over!

- 1.19.0 | TBA!

  Need to add.

- 1.20.0 | TBA!

  Need to add.  

- 1.20.1 | New Shiverchill Mountains! Coming Soon!

  Shiverchill Mountains now has the walking quests.

- 1.22.1 | New Lamplight Town! Coming Soon! Doesn't boot up?!

  This version introduced a new tile-based Lamplight town, and a LOT of new menu designs added in.  

- 1.24.0 | Bounty Hunting Time!

  There's a problem going from Barnacle Cove to Lamplight Town without actually using the map. Bounties are also available as well.
  Pretty safe to say a LOT happened between 1.22.1, and this version.

- 1.25.0 | Sidequests! Coming Soon!

  This version introduced sidequests. You can collect Fireflies, Frozen Somethings and Ice Crystals.

- 1.25.3 | Big Hex now exists! Coming Soon!

  You can now adopt Big Hex by using settings (the options menu) and another way to adopt Big Hex was in 2016 and it's no longer doable. The portal is now on the 2014-2016's map.

- 1.28.1 | Just as many radical changes!

  This version had the updated Skywatch and an updated map screen! This version also introduced achievements and all kinds of
  other, more subtle changes. This version is another one that I really recommend trying out, another definitive update.  

- 1.29.0 | New Bonfire Spire!

  This version had the updated Bonfire Spire! This version also introduced the say something thing on the chat feature. This version is also another one that I really recommend trying out, another definitive update.

- 1.30.0 | Same as 1.29.0!

  TBA

- 1.34.0 | Lamplight Town is now reborn-a-new!
  
  This version had a brand-new tutorial, which is the base for the MODERN DAY Prodigy tutorial, even though it's been changed in multiple ways, the resemblance is
  uncanny! This also brought in the modern-style Lamplight Town, now some things are arranged differently, but it's still essentially the same. 
  The pet buddy shop is still currently closed. The icons on the bar are also redesigned.  

- 1.35.2 | Pumpkinfest!?
 
  Happy Halloween, plus the toolbar has gotten a new design. Candy Corn can be used instead of gold to buy anything from The Pumpkinfest Shop!

- 1.37.0 | Winterfest!?

  This version has Winterfest, which is pretty cool if you're interested in that. You can now buy pet buddies from the pet buddy shop. The Daily Login Bonus is also available. You don't have to complete Firefly Forest first to go to any zone. Going from Firefly Forest to Shiverchill Mountains, Shiverchill Mountains to Skywatch, Skywatch to Bonfire Spire, Bonfire Spire to Barnacle Cove, Bonfire Spire to Skywatch, Skywatch to Shiverchill Mountains, and Shiverchill Mountains to Firefly Forest without actually using the map is now impossible.

- 1.39.0 | Crazy battle update!?

  This version introduced a refresh on the battle system, with a new health meter not using hearts, but a health bar! It also shows how much damage is dealt in
  each attack, and introduces critical hits. This also has leveling up mid-battle and some new animations to accompany this! This version also has the Firefly Forest reskin,
  which looks really cool!

- 1.46.2 | Potions and Epic Attacks!?

  This version added not only Potions, but was also the first to have Epic Attacks! The tutorial route was also re-worked a bit. Rather than going through the heart of
  Lamplight town, this version just passes through the shop district, and is overall a less drawn-out tutorial.

- 1.50.0 | Lots of significant changes added in!

  This version has a lot of significant changes, For starters EPIC ATTACKS are added now, along with some new battle sound effects! The tutorial route was also re-worked
  a bit, and now it uses a shorter route that doesn't take you through Lamplight Square and is overall less drawn-out. This is yet another version I truly recommend,
  in my opinion, it is one of the definitive Prodigy versions.

- 1.51.0 | NEW AVATAR!? EW!

  This update has always been a bit controversial... The avatar was completely reworked in this version, and instead of using sprite sheets, it used atlases. This means
  that the animations could be a LOT smoother than before! However, it just plain didn't have the same amount of detail that the previous avatar had.
  The loading system also got a bit of internal reworking in this update, meaning that some icons would have some loading issues, which is the main reason I
  recommend 1.50.0 over this update.

- 1.52.0 | TBA!

  Need to add...

- 1.55.0 | TBA! (Coming Soon!)

  I'm still working on the colon token incident in game.min.js. 

- 1.60.0 | ARGH! OUR SHIP IS SINKING MATEYS! I THINK WE MIGHT WRECK!

  Ahem - anyway, this update was one of the first with the Starlight Festival! This update also introduced the new in-game chat, although it's not a big deal anyway since
  this is in offline mode... Ooh! Also, this brought in Shipwreck Shore, replacing Barnacle Cove! This also has Pippet Battles!

- 1.70.0 | Huh I can dab now? Cool

  This update added in some quest progress popups for when you enter the following areas: Firefly Forest, Shiverchill Mountain, Skywatch, Bonfire Spire, and Shipwreck Shore.
  Pretty nifty, but is there anything significant- Also the Dark Tower got completely redesigned with a tileset model- COUNT ME IN!
  Oh and aside from all that stuff, you can dab now. Does that mean it's coming back- Also there's Winterfest, and 4 new epics are added in now. Pretty nifty.

- 1.80.0 | A battle overhaul!?

  Wow, that was quick- Huh. Ok - Anyway, the battle got changed a bit so that instead of having up to 4 pets, you only have up to 2. HOWEVER
  They are all on screen at the same time and the battle background uses tilesets now.

- 1.90.0 | Bunnies Vs. Foxes!

  The leaderboards got overhauled a bit and overall look way brighter than before. Also, the Dyno Dig Oasis quests are phased out now, Professor Scoog is now a shopkeeper.
  This version also includes some general stability improvements, and the first Springfest in Prodigy was also here. Also a lot of new music was added in!

- 2.0.0 | Coming Soon!

  Coming Soon!

- 2.6.0 | Another battle overhaul!

  What? Again? That was quick- Ah don't worry about it, that'll be the last major revision for the next 4 and a half years! Whew, thank goodness. Anyway, this marks a point
  where a lot of previously bitmap-based fonts start to get replaced by vector fonts. There are also a lot of more obscure and internal changes but who cares about that, the battle revision is the focus here. Anyway to go into more detail about the battle revision, basically everything has a LOT of health now, attacks do more damage, higher leveled players/pets are way more powerful, and relics and wands have spells associated with them rather than choosing them in a spellbook. The older spells also were completely phased out in favor of newer spells with smoother animation, but that's the gist.

- 2.20.0 | Getting some gale force winds here guys!

  This update has the academy! You'll be able to go into the archives, meet Gale, and do all sorts of stuff there. There is also a lot of GUI changes here. The character
  name now uses a vector font now. The settings menu also got a glow-up! Also, this version happens to be really stable. Like REALLY stable. I definitely recommend giving
  it a shot!

- 2.30.0 | New Nicknames!

  This update has new nicknames and Winterfest! The nicknamer thing also got redesigned.

- 2.45.0 | The Earth Tower!

  The Earth Tower is back in business and plus, you can have All-Out-Attack once the energy's full.

- 2.50.0 | R.I.P. The Lost Island 2017-2019!

  The Lost Island is gone, but is still accessible due to codes. 

- 2.65.0 | Coming Soon!

  Need to add...

- 3.0.0 | It's actually a debug mode! Coming Soon! Credits to Slappygig/Toonigy and Prodigy Education!

 (This is in beta) Wizards like me found this 3.0.0 version in the archives, it was labeled, "DEBUG BUILD" and once we enabled debug and during our fix it up stages, you are free to play around this game so Now its a sandbox mode so lots of interesting stuff!

- 1.30.R | Credits to Craftersshaft and more!

 This is now a fixed version of Prodigy Reborn 1.0. I made Plumber's Cap unobtainable and member only due to the hair loss which means your hair style is set to hair style 0 from New Prodigy.

- 6.136.0 | Coming Soon! NEW AVATAR!? EW! 2

  New wizard looks and other new stuff. Making faces for free is no longer possible. You can now battle the Puppet Master. New unknown outfit: seafoam tank top with fuchsia underwear.

- 6.139.0 | Coming Soon! Merchant Art Update!

  The merchants were redesigned.  

- Prodigy Reborn 1.0 | Not Recommended (If using the one that isn't the fixed version)!

  Too many bugs for the saves in later than April 2020. The saves don't even work in here without the bugs. At least the going home error doesn't occur for any 1-30-0 saves. The successor to this is My Prodigy Ultimate Edition http://opxpmuser.byethost7.com. The fixed version: https://xpmuser.github.io/oldprodigy/1-30-R/ 

- My Prodigy Definitive Editions List | More!

 - Prodigyde (1.10.0) | Defintive Edition on March 2015!

   The bots still exist, but you still can't view their player cards. The house is unavailable due to the house files from 2014-2015 being gone since 2019. You can now buy lots of items from the Prodigy Store. The Dyno Dig Oasis quest data's still viewable even if all the quests are completed. The tutorial is resettable, but only TutorialBattleOne and the end of the tutorial are doable. You can also go to Lamplight Square from the Dark Tower without using the map. In The Tree has been renamed to 'The Tiles Workout Room' and it's also a place to take your character for walks. Going home from the map will take you here instead. There are a few more coliseum opponents. https://xpmuser.github.io/oldprodigy/oldprodigyde/undefined/

 - Pde2015 (1.11.0) | Extra Coliseum Opponents!

   The Prodigy Store is still the same as earlier than summer 2015. Even more coliseum opponents are ready to battle. This version also has the fake leaderboards that's private back in April 2015. In the Tree has been renamed to 'The Walking Room' and parts of core.png can be seen. 
   
 - Oldprodigyde (1.16.5) | Fake Multiplayer Mode!

   You can now view copies of bots' player cards. The bots don't talk or leave the zone. The bot's house is your house. The gift means Rickroll and the battle icon can give you a video of Good Time by Owl City. The map does nothing in the player menu. The bots are also able to go to your house. You can also open this version by clicking on the member icon that's on the top left corner in pde2015 if you're a non-member.

 - Pde (1.18.2) | 2014-2015's Firefly Forest!

   2014-2015's Firefly Forest is still accessible which means that you'll learn the Earthsprite spell.

 - Pdemar2016 (1.24.0) | TBA

   TBA    

 - Pde2016 (1.30.0) | First Definitive Edition with the speed mods?!

   TBA

 - Pdewinter2016 (1.37.0) | TBA

   TBA
   
 - Pdenot1.50.0 (1.46.2) | The Rebalanced Battles Mod is a no-go!?

   This also supports modpacks except for the rebalanced battles mod due to crashes during the battle. The only mods I recommend using are Walk Speed and Fast Game Speed.

 - Pde1.51.0 (1.51.0) | 2017-2022's Avatar!

   Some people didn't like the 2017-2022 avatar, but I had trouble replacing the character back to 2013-2017. 

 - Pde2017 (1.60.0) | TBA

   TBA

 - Pde2019 (2.45.0) | Titans!

   You can now battle the Titan if you use any of the following links:
   1. https://xpmuser.github.io/oldprodigy/pde2019/?mods=ImitationTitan/
   2. https://xpmuser.github.io/oldprodigy/pde2019/?mods=WalkSpeed,ImitationTitan/
   3. https://xpmuser.github.io/oldprodigy/pde2019/?mods=FastGameSpeed,ImitationTitan/
   4. https://xpmuser.github.io/oldprodigy/pde2019/?mods=WalkSpeed,FastGameSpeed,ImitationTitan/   

- Daboss7173's Prodigy Definitive Edition | Closed Source! http://oldprodigy.byethost5.com/play/

  This is in 1.50.0 mode. Multiplayer Mode is coming soon! You can't even break the 4th wall due to it being closed source instead of open source. You can only use the saves from this version. Signing in with Google makes the account work the same way as on the original Prodigy site making you imagine you're in 2017 with just the transformotron removed. You have to get luckier than ever to get the Trialmaster's Gear from The Lost Island. 2017 in 2023-present!

- My Education Edition Versions:

  - 1.10.0 Mode | Directions of Files! http://op1100.byethost9.com/?i=1

    This version doesn't have the question interface, but it's bypassed. I fixed the directions of files bugs. This is the second Education Edition on Byethost. Play this version during your free time.
  
  - 1.11.0 Mode | Directions of Files! http://op2015.byethost5.com/?i=1

    This version doesn't have the question interface, but it's bypassed. I fixed the directions of files bugs. This is the first Education Edition on Byethost. The E-Nigma hat has been fixed. Play this version during your free time.
    
- My Ultimate Edition Versions:
  
 - My Ultimate Edition (1.0.0) | 1.10.0?!

    I installed the questdata and the quests to fix the boredom.

 - My Ultimate Edition (1.2.0) | 1.10.0?!

    Welcome to the Cloud Zone!
   
 - My Ultimate Edition (1.5.0) | 1.10.0?!

    Dark Tower is included in the game.
   
 - My Ultimate Edition in 1.10.0 Mode | 2014-2015's plot?! http://op2015.byethost7.com/ http://op2015.byethost7.com/play/

   2 versions can open from the map.
   
 - My Ultimate Edition in 1.16.5 Mode | Like oldprodigyde!? http://oppickles.byethost14.com/

   This has fake Multiplayer Mode.

 - My Ultimate Edition in 1.17.0 Mode | http://op1170.byethost32.com/play/?i=1

   This also has fake Multiplayer Mode.  

 - My Ultimate Edition in 1.24.0 Mode | R.I.P. Fake Multiplayer Mode! http://op2016.byethost8.com/play/?i=1

   Fake Multiplayer Mode is no longer restorable. The relics can now be bought from Plank's Tanks in Barnacle Cove, but they do nothing at all which is why they're only some worlds' logos.

 - My Ultimate Edition in 1.30.0 Mode | The first byethost site to have the open source version of Old Prodigy!

   You'll get this version from the map in my 1.10.0 Mode Ultimate Edition.  

 - My Ultimate Edition in 1.46.2 Mode | Goodbye Robolymics...

   It got reupdated again. New Update: Parts of Clockwork Town covered some of the things that couldn't be installed. The Rebalanced Battles mod's a no-go for here as well. I'm still working on Sign in with Google, Clockwork Town, etc.

 - My Ultimate Edition in 1.80.0 Mode | 

   Tba. http://op2018.byethost4.com/play/

- Prodigy House Simulator (oldprodigyde/undefined/) | Your character is at home.

  You can get this version from the map on one of my Definitive Editions and one of my Ultimate Editions. The saves from March/Early June 2015 can't go home.
    
- The Current Version | The Actual Multiplayer Mode!
  
  https://play.prodigygame.com/ Time Travel to the past to get the Real Multiplayer Mode for the earlier versions!

- Prodigy Insider Preview (Release Preview) | Coming Soon!

  Coming Soon!

- Prodigy Insider Preview (Beta Channel) | Coming Soon!

  Coming Soon!

- Prodigy Insider Preview (Dev Channel) | Coming Soon!

  Coming Soon!

- Prodigy Insider Preview (Canary Channel) | Coming Soon!

  The lights have now removed the shadows. Too many art updates and more walkways! The house items are also interactable again.

You can choose any version you want!
